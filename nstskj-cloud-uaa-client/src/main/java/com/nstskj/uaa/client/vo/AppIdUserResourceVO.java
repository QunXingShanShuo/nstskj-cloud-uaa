package com.nstskj.uaa.client.vo;

import com.langangkj.core.model.BaseSysApiResource;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project ymdx-saas-uaa
 * @date 2020/10/22 10:14
 * @Description
 */
@Data
public class AppIdUserResourceVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 手机号
     */
    private String mobile;

    /**
     * appId
     */
    private String userAppId;

    /**
     * 用户公钥的base64值
     */
    private String userPublicKey;

    /**
     * 用户私钥的base64值
     */
    private String userPrivateKey;

    /**
     * 客户端公钥的base64值
     */
    private String clientPublicKey;

    /**
     * 版本 用于选择路由服务
     */
    private String userVersion;

    /**
     * 染色
     */
    private Map<String, String> stainingMap;

    /**
     * 权限
     */
    private List<BaseSysApiResource> sysApiResourceList;

}
