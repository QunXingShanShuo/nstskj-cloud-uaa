package com.nstskj.uaa.service.service.impl;

import com.langangkj.db.service.impl.SuperServiceImpl;
import com.nstskj.uaa.service.service.ISaasAppApiResourceRelationService;
import com.nstskj.uaa.service.entity.SaasAppApiResourceRelation;
import com.nstskj.uaa.service.mapper.SaasAppApiResourceRelationMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * appId对应的API资源权限 服务实现类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Slf4j
@Service
public class SaasAppApiResourceRelationServiceImpl extends SuperServiceImpl<SaasAppApiResourceRelationMapper, SaasAppApiResourceRelation> implements ISaasAppApiResourceRelationService {


}
