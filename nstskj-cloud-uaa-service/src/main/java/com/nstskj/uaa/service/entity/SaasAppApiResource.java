package com.nstskj.uaa.service.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * API资源池
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("saas_app_api_resource")
public class SaasAppApiResource implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 资源id
     */
    @TableField("resource_id")
    private Long resourceId;

    /**
     * 资源名称
     */
    @TableField("resource_name")
    private String resourceName;

    /**
     * 资源描述
     */
    @TableField("resource_desc")
    private String resourceDesc;

    /**
     * 请求方法 只能为post
     */
    @TableField("request_method")
    private String requestMethod;

    /**
     * 请求的url
     */
    @TableField("request_url")
    private String requestUrl;

    /**
     * 是否启用 1 启用 0 关闭
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 创建时间戳
     */
    @TableField("gmt_create")
    private LocalDateTime gmtCreate;

    /**
     * 修改时间搓
     */
    @TableField("gmt_modified")
    private LocalDateTime gmtModified;


}
