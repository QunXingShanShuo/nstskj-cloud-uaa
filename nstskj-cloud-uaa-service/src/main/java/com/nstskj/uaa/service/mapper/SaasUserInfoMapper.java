package com.nstskj.uaa.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nstskj.uaa.service.entity.SaasUserInfo;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface SaasUserInfoMapper extends BaseMapper<SaasUserInfo> {

}
