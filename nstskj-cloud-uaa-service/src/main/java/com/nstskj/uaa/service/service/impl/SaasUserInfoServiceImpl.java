package com.nstskj.uaa.service.service.impl;

import com.langangkj.db.service.impl.SuperServiceImpl;
import com.nstskj.uaa.service.mapper.SaasUserInfoMapper;
import com.nstskj.uaa.service.service.ISaasUserInfoService;
import com.nstskj.uaa.service.entity.SaasUserInfo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Service
public class SaasUserInfoServiceImpl extends SuperServiceImpl<SaasUserInfoMapper, SaasUserInfo> implements ISaasUserInfoService {

}
