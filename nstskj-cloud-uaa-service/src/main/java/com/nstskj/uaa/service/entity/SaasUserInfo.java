package com.nstskj.uaa.service.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.util.Map;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "saas_user_info", autoResultMap = true)
public class SaasUserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 账号
     */
    @TableField("username")
    private String username;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 性别 1 男 2 女
     */
    @TableField("sex")
    private String sex;

    /**
     * 版本 用于选择路由服务
     */
    @TableField("user_version")
    private String userVersion;

    /**
     * 染色
     */
    @TableField(value = "staining_map", typeHandler = JacksonTypeHandler.class)
    private Map<String, String> stainingMap;

    /**
     * 状态
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 创建时间戳
     */
    @TableField("gmt_create")
    private LocalDateTime gmtCreate;

    /**
     * 修改时间搓
     */
    @TableField("gmt_modified")
    private LocalDateTime gmtModified;
}
