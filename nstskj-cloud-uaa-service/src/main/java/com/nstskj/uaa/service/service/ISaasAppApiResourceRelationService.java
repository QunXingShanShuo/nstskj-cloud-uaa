package com.nstskj.uaa.service.service;

import com.langangkj.db.service.ISuperService;
import com.nstskj.uaa.service.entity.SaasAppApiResourceRelation;

/**
 * <p>
 * appId对应的API资源权限 服务类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface ISaasAppApiResourceRelationService extends ISuperService<SaasAppApiResourceRelation> {

}
