package com.nstskj.uaa.service.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.util.Map;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户下的appId及秘钥
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "saas_user_app_info", autoResultMap = true)
public class SaasUserAppInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 用户appid
     */
    @TableField("user_app_id")
    private String userAppId;

    /**
     * 用户公钥的base64值
     */
    @TableField("user_public_key")
    private String userPublicKey;

    /**
     * 用户私钥的base64值
     */
    @TableField("user_private_key")
    private String userPrivateKey;

    /**
     * 客户端公钥的base64值
     */
    @TableField("client_public_key")
    private String clientPublicKey;

    /**
     * 版本 用于选择路由服务
     */
    @TableField("user_version")
    private String userVersion;

    /**
     * 染色
     */
    @TableField(value = "staining_map", typeHandler = JacksonTypeHandler.class)
    private Map<String, String> stainingMap;

    /**
     * 是否启用
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 创建时间戳
     */
    @TableField("gmt_create")
    private LocalDateTime gmtCreate;

    /**
     * 修改时间搓
     */
    @TableField("gmt_modified")
    private LocalDateTime gmtModified;


}
