package com.nstskj.uaa.service.service;


import com.langangkj.core.model.BaseSysApiResource;
import com.langangkj.core.response.wrap.PageVo;
import com.nstskj.uaa.client.vo.AppIdUserResourceVO;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project ymdx-saas-uaa
 * @date 2020/10/22 10:22
 * @Description
 */
public interface UserAppResourceFeignService {
    /**
     * 根据appId查询用户信息及api权限
     *
     * @param appId
     * @return
     */
    AppIdUserResourceVO getAppIdUserInfoAndApiResourceByAppId(String appId);

    /**
     * 得到当前系统中所以的 api资源
     *
     * @return
     */
    PageVo<BaseSysApiResource> getAllApiResources();

}
