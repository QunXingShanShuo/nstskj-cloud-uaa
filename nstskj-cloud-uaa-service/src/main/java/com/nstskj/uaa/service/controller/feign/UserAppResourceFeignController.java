package com.nstskj.uaa.service.controller.feign;

import cn.hutool.core.lang.Assert;
import com.langangkj.core.model.BaseSysApiResource;
import com.langangkj.core.response.Result;
import com.langangkj.core.response.wrap.PageVo;
import com.nstskj.uaa.client.vo.AppIdUserResourceVO;
import com.nstskj.uaa.service.service.UserAppResourceFeignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project ymdx-saas-uaa
 * @date 2020/10/22 10:00
 * @Description
 */
@Slf4j
@Api(value = "用户appId权限控制", tags = "feign调用的用户appId权限控制")
@RestController
@RequestMapping(UserAppResourceFeignController.BASE_URL)
public class UserAppResourceFeignController {

    public static final String BASE_URL = "/v1/feign/userAppResource";

    @Autowired
    private UserAppResourceFeignService userAppResourceFeignService;

    /**
     * 根据appId查询用户信息及api权限
     *
     * @param appId
     * @return
     */
    @ApiOperation(value = "根据appId查询用户信息及api权限")
    @RequestMapping("/getAppIdUserInfoAndApiResourceByAppId/{appId}")
    public Result<AppIdUserResourceVO> getAppIdUserInfoAndApiResourceByAppId(@PathVariable("appId") String appId) {
        AppIdUserResourceVO appIdUserResourceVO = userAppResourceFeignService.getAppIdUserInfoAndApiResourceByAppId(appId);
        Assert.notNull(appIdUserResourceVO, "查询失败");
        return Result.succeed(appIdUserResourceVO);
    }

    /**
     * 得到当前系统中所以的 api资源
     *
     * @return
     */
    @ApiOperation(value = "得到当前系统中所以的 api资源")
    @RequestMapping("/getAllApiResources")
    public Result<PageVo<BaseSysApiResource>> getAllApiResources() {
        PageVo<BaseSysApiResource> allApiResources = userAppResourceFeignService.getAllApiResources();
        Assert.notNull(allApiResources, "查询失败");
        return Result.succeed(allApiResources);
    }


}
