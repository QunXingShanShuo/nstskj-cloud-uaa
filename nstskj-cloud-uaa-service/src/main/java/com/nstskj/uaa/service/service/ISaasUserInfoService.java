package com.nstskj.uaa.service.service;

import com.langangkj.db.service.ISuperService;
import com.nstskj.uaa.service.entity.SaasUserInfo;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface ISaasUserInfoService extends ISuperService<SaasUserInfo> {

}
