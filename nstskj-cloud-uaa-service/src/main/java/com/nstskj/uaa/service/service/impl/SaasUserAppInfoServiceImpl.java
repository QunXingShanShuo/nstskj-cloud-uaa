package com.nstskj.uaa.service.service.impl;

import com.langangkj.db.service.impl.SuperServiceImpl;
import com.nstskj.uaa.service.service.ISaasUserAppInfoService;
import com.nstskj.uaa.service.entity.SaasUserAppInfo;
import com.nstskj.uaa.service.mapper.SaasUserAppInfoMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户下的appId及秘钥 服务实现类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Service
public class SaasUserAppInfoServiceImpl extends SuperServiceImpl<SaasUserAppInfoMapper, SaasUserAppInfo> implements ISaasUserAppInfoService {

}
