package com.nstskj.uaa.service.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project ymdx-saas-uaa
 * @date 2020/10/21 14:59
 * @Description
 */
@Slf4j
@Api(value = "用户appId权限控制", tags = "用户appId权限控制")
@RestController
@RequestMapping(UserAppResourceController.BASE_URL)
public class UserAppResourceController {

    public static final String BASE_URL = "/v1/userAppResource";


}
