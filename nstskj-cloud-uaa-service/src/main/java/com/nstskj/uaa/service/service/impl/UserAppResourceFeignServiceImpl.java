package com.nstskj.uaa.service.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.langangkj.core.model.BaseSysApiResource;
import com.langangkj.core.response.wrap.PageVo;
import com.langangkj.core.response.wrap.PageVoFactory;
import com.nstskj.uaa.client.vo.AppIdUserResourceVO;
import com.nstskj.uaa.service.entity.SaasAppApiResource;
import com.nstskj.uaa.service.entity.SaasAppApiResourceRelation;
import com.nstskj.uaa.service.entity.SaasUserAppInfo;
import com.nstskj.uaa.service.entity.SaasUserInfo;
import com.nstskj.uaa.service.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project ymdx-saas-uaa
 * @date 2020/10/22 10:22
 * @Description
 */
@Slf4j
@CacheConfig(keyGenerator = "keyGenerator", cacheNames = "userAppResourceFeign")
@Service
public class UserAppResourceFeignServiceImpl implements UserAppResourceFeignService {

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private ISaasUserAppInfoService iSaasUserAppInfoService;

    @Autowired
    private ISaasUserInfoService iSaasUserInfoService;

    @Autowired
    private ISaasAppApiResourceRelationService iSaasAppApiResourceRelationService;

    @Autowired
    private ISaasAppApiResourceService iSaasAppApiResourceService;

    /**
     * @param appId
     * @return
     */
    @Cacheable(unless = "#result == null")
    @Override
    public AppIdUserResourceVO getAppIdUserInfoAndApiResourceByAppId(String appId) {
        log.info("读取appId的信息 {}", appId);
        final AppIdUserResourceVO appIdUserResourceVO = new AppIdUserResourceVO();
        final SaasUserAppInfo saasUserAppInfo = iSaasUserAppInfoService.getOne(
                Wrappers.lambdaQuery(SaasUserAppInfo.class)
                        .eq(SaasUserAppInfo::getUserAppId, appId)
        );
        Assert.notNull(saasUserAppInfo, "appId不存在");
        Assert.isTrue(Boolean.TRUE.equals(saasUserAppInfo.getEnabled()), "appId已关闭");
        BeanUtil.copyProperties(saasUserAppInfo, appIdUserResourceVO);
        final SaasUserInfo saasUserInfo = iSaasUserInfoService.getOne(Wrappers.lambdaQuery(SaasUserInfo.class)
                .eq(SaasUserInfo::getUserId, appIdUserResourceVO.getUserId())
                .eq(SaasUserInfo::getEnabled, true));
        Assert.notNull(saasUserInfo, "用户不存在");
        BeanUtil.copyProperties(saasUserInfo, appIdUserResourceVO, "userVersion", "stainingMap");
        final List<BaseSysApiResource> baseSysApiResources = new ArrayList<>();
        final Set<Long> resourceSet = iSaasAppApiResourceRelationService.list(Wrappers
                .lambdaQuery(SaasAppApiResourceRelation.class)
                .eq(SaasAppApiResourceRelation::getUserAppId, appId)
                .eq(SaasAppApiResourceRelation::getEnabled, true)
                .select(SaasAppApiResourceRelation::getResourceId))
                .parallelStream()
                .map(SaasAppApiResourceRelation::getResourceId)
                .collect(Collectors.toSet());
        if (CollectionUtil.isNotEmpty(resourceSet)) {
            final List<BaseSysApiResource> sysApiResources = iSaasAppApiResourceService.list(Wrappers
                    .lambdaQuery(SaasAppApiResource.class)
                    .eq(SaasAppApiResource::getEnabled, true)
                    .in(SaasAppApiResource::getResourceId, resourceSet)
                    .select(SaasAppApiResource::getResourceId, SaasAppApiResource::getRequestUrl, SaasAppApiResource::getRequestMethod))
                    .parallelStream()
                    .map(obj -> {
                        final BaseSysApiResource baseSysApiResource = new BaseSysApiResource();
                        baseSysApiResource.setId(obj.getResourceId());
                        baseSysApiResource.setPathUrl(obj.getRequestUrl());
                        baseSysApiResource.setPathMethod(obj.getRequestMethod());
                        return baseSysApiResource;
                    }).collect(Collectors.toList());
            baseSysApiResources.addAll(sysApiResources);
        }
        appIdUserResourceVO.setSysApiResourceList(baseSysApiResources);
        return appIdUserResourceVO;
    }

    /**
     * 得到当前系统中所以的 api资源
     *
     * @return
     */
    @Cacheable(unless = "#result == null || #result.getTotal() == 0 ")
    @Override
    public PageVo<BaseSysApiResource> getAllApiResources() {
        List<SaasAppApiResource> list = iSaasAppApiResourceService.list(Wrappers.lambdaQuery(SaasAppApiResource.class)
                .eq(SaasAppApiResource::getEnabled, true)
                .select(SaasAppApiResource::getResourceId,
                        SaasAppApiResource::getRequestUrl,
                        SaasAppApiResource::getRequestMethod));
        PageVo<BaseSysApiResource> baseSysApiResourcePageVo = PageVoFactory.originalBuilder(list, obj -> {
            final BaseSysApiResource baseSysApiResource = new BaseSysApiResource();
            baseSysApiResource.setId(obj.getResourceId());
            baseSysApiResource.setPathUrl(obj.getRequestUrl());
            baseSysApiResource.setPathMethod(obj.getRequestMethod());
            return baseSysApiResource;
        });
        log.info("查询所以api资源  资源数量 {}", baseSysApiResourcePageVo.getPageTotal());
        return baseSysApiResourcePageVo;
    }

}
