package com.nstskj.uaa.service.service;

import com.langangkj.db.service.ISuperService;
import com.nstskj.uaa.service.entity.SaasAppApiResource;

/**
 * <p>
 * API资源池 服务类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface ISaasAppApiResourceService extends ISuperService<SaasAppApiResource> {

}
