package com.nstskj.uaa.service.service;

import com.langangkj.db.service.ISuperService;
import com.nstskj.uaa.service.entity.SaasUserAppInfo;

/**
 * <p>
 * 用户下的appId及秘钥 服务类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface ISaasUserAppInfoService extends ISuperService<SaasUserAppInfo> {

}
