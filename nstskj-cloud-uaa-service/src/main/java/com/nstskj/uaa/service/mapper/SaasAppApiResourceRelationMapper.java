package com.nstskj.uaa.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nstskj.uaa.service.entity.SaasAppApiResourceRelation;

/**
 * <p>
 * appId对应的API资源权限 Mapper 接口
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface SaasAppApiResourceRelationMapper extends BaseMapper<SaasAppApiResourceRelation> {

}
