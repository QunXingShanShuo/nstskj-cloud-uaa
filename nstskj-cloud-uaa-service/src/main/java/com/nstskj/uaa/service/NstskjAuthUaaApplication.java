package com.nstskj.uaa.service;

import com.langangkj.common.ribbon.annotation.EnableRequestHeaderFeignInterceptor;
import com.langangkj.core.annotation.EnableCollectionHeaderFilter;
import com.langangkj.core.annotation.EnableGlobalExceptionHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author ZhouChuGang
 * @version 1.0
 * @project nstskj-cloud-uaa-service
 * @date 2020/10/22 10:00
 * @Description
 */
@EnableCollectionHeaderFilter
@EnableTransactionManagement
@EnableGlobalExceptionHandler
@SpringBootApplication
@EnableDiscoveryClient
@EnableRequestHeaderFeignInterceptor
@MapperScan("com.nstskj.uaa.service.mapper")
public class NstskjAuthUaaApplication {

    public static void main(String[] args) {
        SpringApplication.run(NstskjAuthUaaApplication.class, args);
    }

}
