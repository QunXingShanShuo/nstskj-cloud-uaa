package com.nstskj.uaa.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nstskj.uaa.service.entity.SaasUserAppInfo;

/**
 * <p>
 * 用户下的appId及秘钥 Mapper 接口
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface SaasUserAppInfoMapper extends BaseMapper<SaasUserAppInfo> {

}
