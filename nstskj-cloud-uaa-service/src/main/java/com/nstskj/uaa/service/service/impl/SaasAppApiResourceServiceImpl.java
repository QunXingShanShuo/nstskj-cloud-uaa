package com.nstskj.uaa.service.service.impl;

import com.langangkj.db.service.impl.SuperServiceImpl;
import com.nstskj.uaa.service.service.ISaasAppApiResourceService;
import com.nstskj.uaa.service.entity.SaasAppApiResource;
import com.nstskj.uaa.service.mapper.SaasAppApiResourceMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * API资源池 服务实现类
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
@Service
public class SaasAppApiResourceServiceImpl extends SuperServiceImpl<SaasAppApiResourceMapper, SaasAppApiResource> implements ISaasAppApiResourceService {

}
