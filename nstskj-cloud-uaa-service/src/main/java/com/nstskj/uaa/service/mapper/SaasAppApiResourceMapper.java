package com.nstskj.uaa.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nstskj.uaa.service.entity.SaasAppApiResource;

/**
 * <p>
 * API资源池 Mapper 接口
 * </p>
 *
 * @author gang
 * @since 2020-10-21
 */
public interface SaasAppApiResourceMapper extends BaseMapper<SaasAppApiResource> {

}
